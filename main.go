package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/user"
	"runtime"
	"time"
)

type Client struct {
	HttpClient http.Client
	URL        string
	Body       string
}

func (c *Client) Post() (r *http.Response, err error) {
	req, err := http.NewRequest("POST", c.URL, bytes.NewBufferString(c.Body))
	if err != nil {
		return nil, fmt.Errorf("ERROR::http.NewRequest::%v\n", err)
	}
	r, err = c.HttpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("ERROR::client.Do(r)::%v\n", err)
	}
	return r, err
}

type System struct {
	OS            string
	CPU_THREADS   int
	TOTAL_MEMORY  int
	IP            string
	COMPUTER_NAME string
	USER_NAME     string
}

func main() {
	var url string
	flag.StringVar(&url, "url", "http://10.0.0.61:8844", "URL")
	flag.Parse()
	c := Client{
		URL:  url,
		Body: func() string {
			jb, err := json.MarshalIndent(System{
				OS:           runtime.GOOS,
				CPU_THREADS:  runtime.NumCPU(),
				TOTAL_MEMORY: sysMem(),
				IP: func() string {
					a, err := net.InterfaceAddrs()
					if err != nil {
						log.Printf("ERROR::net.InterfaceAddrs()::%v\n", err)
						return ""
					}
					for _, address := range a {
						if ip, ok := address.(*net.IPNet); ok && !ip.IP.IsLoopback() {
							if ip.IP.To4() != nil {
								return ip.IP.String()
							}
						}
					}
					return ""
				}(),
				COMPUTER_NAME: func() string {
					hn, err := os.Hostname()
					if err != nil {
						fmt.Printf("ERROR::os.Hostname()::%v\n", err)
						return "ERROR::unable to get hostname"
					}
					return hn
				}(),
				USER_NAME: func() string {
					u, err := user.Current()
					if err != nil {
						fmt.Printf("ERROR::user.Current()::%v\n", err)
						return "ERROR: unable to get username"
					}
					return u.Username
				}(),
			}, "", " ")
			if err != nil {
				fmt.Printf("ERROR::json.Marshal(SYSTEM)::%v\n", err)
				return "ERROR:: unable to marshal SYSTEM"
			}
			return string(jb)
		}(),
		HttpClient: http.Client{
			Transport:     nil,
			CheckRedirect: nil,
			Jar:           nil,
			Timeout:       time.Duration(10) * time.Second,
		},
	}
	if r, err := c.Post(); err != nil {
		log.Printf("ERROR::c.Post()::%v\n", err)
		return
	} else {
		bs, err := func(r *http.Response) (string, error) {
			bb, err := ioutil.ReadAll(r.Body)
			if err != nil {
				return "", fmt.Errorf("ERROR::ioutil.ReadAll(r.Body)::%v\n", err)
			}
			return string(bb), nil
		}(r)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(bs)
	}
}
